Histoires d'un ~~salarié~~ collègue perfectionniste sous pression
=================================================================

**7 octobre 2020 - GitLab France Virtual Meetup**

_S'organiser à petite échelle avec GitLab_

---

Une expérience d'organisation inspirée par la lecture (partielle) du [Handbook GitLab](https://about.gitlab.com/handbook/) et construite avec l'arrivée de mon premier collègue. Un cas d'usage de GitLab qui concerne les (très) petites équipes/organisations.
À base de [sous-groupes](https://docs.gitlab.com/ce/user/group/subgroups/), [CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/), [`pages`](https://about.gitlab.com/stages-devops-lifecycle/pages/), documentation unique, [`pip`](https://pip.pypa.io/en/stable/) et [Django](https://djangoproject.com).

![GitLabFr logo](img/gitlabfr.png)
