### Histoires d'un collègue perfectionniste sous pression

![GitLab virtual meetup logo](img/gitlab-virtual-meetup-s.png)

_7 octobre 2020 - Frédéric Zind - GitLab France Virtual Meetup_

+++

S'organiser à (très) petite échelle avec GitLab

___

![gitlab logo](img/gitlab.png)
![mkDocs logo](img/mkdocs.png)
![git logo](img/git.png)
![python logo](img/python.png)
![django logo](img/django.png)
![plantuml logo](img/plantuml.png)
![pytest logo](img/pytest.png)

___

### ➡️ [`gitlab.com/forga`](https://gitlab.com/forga)

---

## 👨 Qui suis-je ?

@ul
- technicien en mécanique
- logistique & assistance
- gestion d'humains, clients & matériels
- 2018: apprenti charmeur de serpent 🐍
@ulend

+++

![MF2i logo](img/200-mf2i.png)
![double arrow right](img/200-double-arrow-right.png)
![OVHcloud logo](img/200-ovh.png)

---

# ⚗

## ⚠️ Avertissement ⚠️

@ul
- projet expérimental en cours de développement
- certaines _choses_ ont été mis en place
- d'autres pas
- …mais je les présente quand même
@ulend

---

# 💡 L'idée 

+++

![GitLab handbook](img/200-gitlab-handbook.png)

+++

![GitLab handbook](img/200-gitlab-handbook.png)
![1er collègue](img/gnome-adwaita-207-user-new-derivative.png)

+++

![GitLab handbook](img/200-gitlab-handbook.png)
![1er collègue](img/gnome-adwaita-207-user-new-derivative.png)
![coronavirus logo](img/200-coronavirus.png)

+++

![GitLab handbook](img/200-gitlab-handbook.png)
![1er collègue](img/gnome-adwaita-207-user-new-derivative.png)
![coronavirus logo](img/200-coronavirus.png)
![Vignette du tuto vidéo : Comment ranger ses dépôts GitLab?](img/compagnon-devops-tuto-ranger-gitlab.png)

---

# 📈 Objectifs

@ul
- 🗃 ranger
- 📝 documenter
- ♻️ réutiliser
- 🤝 collaborer
@ulend

---

## 🗃 Ranger

➡️ Groupe GitLab + sous-groupe + dépôt `git`

+++

_Les groupes peuvent contenir…_

…des dépôts `git`

![groupes avec dépôts git](img/group-repo-1.png)

+++

_Les groupes peuvent contenir…_

…des dépôts `git`

![groupes avec dépôts git](img/group-repo-2.png)

+++

_Les groupes peuvent contenir…_

…des utilisateurs

![groupes d'utilisateurs](img/group-user.png)

+++

_Les groupes peuvent contenir…_

…d'autres groupes (sous-groupes)

![sous groupes](img/group-subgroup.png)

+++

![Groups everywhere!](img/groups_everywhere.jpg)

+++

_Les groupes sont…_

… des espaces de noms

@ul
- `https://lab.frogg.it/forga`
- `https://lab.frogg.it/forga/process`
- `https://lab.frogg.it/forga/process/fr`
@ulend

+++

_Les groupes sont…_

… mentionnables

@ul
- `@visitor`
- `@devel`
- `@devel/api`
@ulend

+++

_Les groupes sont…_

… configurables

@ul
- nom, desription, logo
- visibilité (privé/public)
- droit d'accès
- etc.
@ulend

+++

_Les groupes sont…_

… une source transverse

@ul
- _issues_
- _merge requests_
@ulend

+++

@snap[west]
Avant 👎
@snapend

@snap[east]
![liste de dépôt git](img/repo-list.png)
@snapend

+++

@snap[west]
Après 👍
@snapend

@snap[east]
![liste de dépôt git](img/group-list.png)
@snapend

---

## 📝 Documenter

➡️ `MkDocs` + GitLab CI + `pages`

+++

```shell
$tree .
.
├── docs
│   ├── about
│   │   └── legal.md
│   ├── convention
│   │   ├── git.md
│   │   └── python.md
│   ├── human
│   │   ├── code-of-conduct.md
│   │   ├── onboarding.md
│   │   └── python-training-plan.md
│   └── index.md
├── .gitlab-ci.yml
├── mkdocs.yml
├── README.md
└── requirements.txt
```
@[3-13](Le contenu de la documentation)
@[17](La dépendance à MkDocs)
@[15](La config de MkDocs)
@[14](La config de génération du HTML)

+++

```shell
$ cat mkdocs.yml
nav:
    - Accueil: index.md
    - Convention:
        - Git: convention/git.md
        - Python: convention/python.md
    - Humain:
        - Code de conduite: human/code-of-conduct.md
        - Embarquement: human/onboarding.md
        - Formation python: human/python-training-plan.md
    - Info:
        - À propos: about/about.md
        - Légal: about/legal.md
        - Licence: about/license.md
site_name: Notre manuel
site_url: https://forga.gitlab.io/process/fr/manuel/
```

+++

```
image: python:3-slim

before_script:
  - pip install -r requirements.txt

pages:
  script:
  - cp README.md docs/about/about.md
  - cp LICENSE.md docs/about/license.md
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
    - production
```

---

## ♻️ Réutiliser

➡️ `git` + `pip` (python)

+++

#### 1 projet client : `forga/customer/acme`
@ul
- utilise les _outils Django_ `forga/tool/django`
    * [`forga/tool/django/core`](https://gitlab.com/forga/tool/django/core)
    * [`forga/tool/django/one-to-one`](https://gitlab.com/forga/tool/django/one_to_one)
@ulend

+++

#### 1 projet client : `forga/customer/acme`
```python
$ git remote -v
core	git@gitlab.com:forga/tool/django/core.git (fetch)
core	git@gitlab.com:forga/tool/django/core.git (push)
origin	git@gitlab.com:forga/customer/acme.git (fetch)
origin	git@gitlab.com:forga/customer/acme.git (push)
```
+++

#### 1 projet client : `forga/customer/acme`
```python
$ find . -maxdepth 1 -type d
.
./.git
./core
```

+++

#### 1 projet client : `forga/customer/acme`
```python
$ cat requirements.txt
asgiref==3.2.10
Django==3.1
pytz==2020.1
sqlparse==0.3.1
git+https://gitlab.com/forga/tool/django/one_to_one.git@stable#egg=one_to_one
```
@[3]
@[6]

+++

### 🏗 Construction de `forga/customer/acme`

+++


```shell
$ git clone git@gitlab.com:forga/tool/django/core.git acme
$ cd acme
$ git remote rename origin core
$ git remote add origin git@gitlab.com:forga/customer/acme.git
$ path/to/python3.7 -m venv ~/.venvs/acme
$ source ~/.venvs/acme/bin/activate
$ pip install -r requirements.txt
$ ./manage.py migrate
$ ./manage.py runserver
```
@[1]
@[2-3]
@[4]
@[5-7](Ajouter le paquet `forga/tool/django/one-to-one`)
@[8-9](🚀 Et voilà !)

---

## 🤝 Collaborer

➡️ GitLab issue + issue template

+++

```shell
$ tree .
.
├── .gitlab
│   └── issue_templates
│       ├── decouverte-projet.md
│       ├── profil-devel-python.md
│       ├── tutorat-gitlab-django.md
│       ├── tutorat-puml.md
│       └── tutorat-pytest-gitlab_ci.md
└── README.md
```

+++

_Issue template_, exemple :

https://gitlab.com/forga/process/fr/embarquement/-/edit/production/.gitlab/issue_templates/tutorat-gitlab-django.md

+++

La personne tutorée :

@ul
- ouvre un ticket à son nom
- coche les tâches au fur et à mesure
@ulend

+++

### ⏳ Un essai en _live_ ?

https://gitlab.com/forga/process/fr/embarquement

---

## Crédits & merci

* icônes des (sous)groupes :  [GNOME/adwaita-icon-theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/blob/production/COPYING)
* icônes drapeaux : [Freepik](https://www.flaticon.com/authors/freepik) sur [www.flaticon.com](https://www.flaticon.com/packs/flags-collection-3)
* Groups everywhere ! : [imgflip.com](https://imgflip.com/memegenerator/13026863/TOYSTORY-EVERYWHERE)
* Christophe Chaudier  : [Comment ranger ses dépôts GitLab?](https://forum.compagnons-devops.fr/t/tuto-gitlab-comment-ranger-ses-depots-gitlab/676?u=freezed)
* Coronavirus logo : [created by mokoland - www.freepik.com](https://www.freepik.com/vectors/template)

---

### Questions , remarques, etc.

# ⁉️

+++

@snap[west]
![QRcode](img/qrcode-pro.zind.fr.png)
@snapend

@snap[south]
http://pro.zind.fr
@snapend

@snap[east]
### Merci !
@snapend
