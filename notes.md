Notes
=====


S'organiser à (très) petite échelle avec GitLab
-----------------------------------------------

- 1er meetup virtuel
- essai de lever de mains
- suite AFPy 23 octobre 20
- qui git ?
- qui gitlab ?
- qui dev django ?
- qui admin ?


Qui suis-je ?
-------------

- libriste pratiquant depuis Debian 3 (woody/2002)
- 2018 veut devenir pro => Python


Job
---

- mars 2019 > mi-septembre 2020
- TPE > Boite légendaire
- clôture d'un cycle, soutenance de fin de poste


L'idée
------

- dev solo mars 2019
- lecture handbook GL
- 1er collègue mai 2020 (dé-confinement)
- même quand on y croit, il faut de l'énergie pour changer


Objectifs
---------

- 2 derniers objectifs inversé pour la présentation


Documenter
----------

- source unique de vérité (single source of truth)
- publication la plus simple : «everyone can contribute»
    - Sid fixing typo : www.linkedin.com/posts/derfabianpeter_opensource-activity-6717786219301892097-jr6U
- => Générateur de site statique (SSG)
- mkdocs vs pelican
- mkdocs vs about.gitlab.com
- $tree .
    * 4p
- $ cat mkdocs.yml
    * nav: arborescence
    * site_name, site_url, etc.
- .gitlab-ci.yaml
    * Une petite image python
    * Installation de MkDocs
    * On génère 2 pages
    * Publication du HTML
    * autres possibilité : linter, checker, CSS/JS, etc.


Réutiliser
----------

- vieux rêve
- TODO du précédent meetup
- forga/customer/acme
    * forga/tool/django/core : wsgi, settings, …
    * forga/tool/django/one-to-one :


Collaborer
----------

- …


Questions , remarques, etc.
---------------------------

- /!\ attention entretien filmé
